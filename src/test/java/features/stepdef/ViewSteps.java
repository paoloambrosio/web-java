package features.stepdef;

import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import utils.page.WebDriverFactory;

import static org.junit.Assert.assertTrue;

public class ViewSteps {

    @Autowired
    private WebDriverFactory webDriverFactory;

    @Then("^I should see \"([^\"]+)\"$")
    public void shouldSee(final String msg) {
        assertTrue(webDriverFactory.getCurrentWebDriver().getPageSource().contains(msg));
    }
}
