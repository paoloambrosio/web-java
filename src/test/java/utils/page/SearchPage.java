package utils.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SearchPage extends BrowserPage {

    public void visit() {
        webDriver().get(BASE_URL + "/search");
    }

    public void fillQueryField(final String query) {
        final WebElement element = webDriver().findElement(By.id("query"));
        element.sendKeys(query);
    }

    public void submitSearch() {
        webDriver().findElement(By.id("submit")).submit();
    }

    public List<String> searchResults() {
        final List<WebElement> pageElements = webDriver().findElements(By.cssSelector("ol.results li"));
        final List<String> searchResults = new ArrayList<String>(pageElements.size());
        for (final WebElement e : pageElements) {
            searchResults.add(e.getText());
        }
        return searchResults;
    }

}